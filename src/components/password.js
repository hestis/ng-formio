var fs = require('fs');

module.exports = function(app) {
  app.config([
    'formioComponentsProvider',
    function(formioComponentsProvider) {
      formioComponentsProvider.register('password', {
        title: 'Password',
        template: 'formio/components/password.html',
        tableView: function() {
          return '--- PROTECTED ---';
        },
        controller: [
          '$scope',
          function($scope) {
            $scope.eyeIconState = 'open';
            $scope.togglePasswordMask = function() {
              if ($scope.eyeIconState === 'open') {
                $scope.eyeIconState = 'close';
                $scope.component.inputType = 'text';
              }
              else {
                $scope.eyeIconState = 'open';
                $scope.component.inputType = 'password';
              }
            };
          }
        ],
        settings: {
          input: true,
          tableView: false,
          inputType: 'password',
          label: '',
          key: 'passwordField',
          enableShowHide: true,
          placeholder: '',
          prefix: '',
          suffix: '',
          protected: true,
          persistent: true,
          hidden: false,
          clearOnHide: true
        }
      });
    }
  ]);

  app.run([
    '$templateCache',
    'FormioUtils',
    function(
      $templateCache,
      FormioUtils
    ) {
      $templateCache.put('formio/components/password.html', FormioUtils.fieldWrap(
        fs.readFileSync(__dirname + '/../templates/components/password.html', 'utf8')
      ));
    }
  ]);
};
