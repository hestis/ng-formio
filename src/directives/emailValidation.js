module.exports = function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attrs, ctrl) {
      if (!ctrl) {
        return false;
      }

      function isValidEmail(value) {
        if (!value) {
          return false;
        }
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.exec(value) != null;
      }

      scope.$watch(ctrl, function() {
        ctrl.$validate();
      });

      ctrl.$validators.email = function(modelValue, viewValue) {
        return isValidEmail(viewValue);
      };
    }
  };
};
