module.exports = function() {
  return {
    restrict: 'EA',
    scope: {
      shouldRender: '<',
      spinnerClass: '@'
    },
    templateUrl: 'formio/spinner.html'
  };
};

